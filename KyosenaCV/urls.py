from django.urls import path
from . import views

app_name = 'KyosenaCV'

urlpatterns = [
    path('', views.index, name='index'),
    path('Resume/', views.Resume, name='Resume'),
    path('AboutMe/', views.AboutMe, name = 'AboutMe'),
    # dilanjutkan ...
]