from django.shortcuts import render

# Create your views here.
def index(request):
    return render(request, 'index.html')

def Resume(request):
    return render(request, 'Resume.html')

def AboutMe(request):
    return render(request,'AboutMe.html')